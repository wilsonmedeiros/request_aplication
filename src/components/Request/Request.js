import React, { useState } from 'react'
import {Row, Col, Card, CardBody} from 'reactstrap'
export default function Request(props){


    
const requestBody = () => {
    return(
        props.state.terno.map((item, index) => {
            let layout
            if (item.funcao === "AUX") {
                return (
                <>
                    < Row key={index} className="text-center line">
                        <Col className="font-30 mt-2 preview-title">GERAL</Col>
                    </Row>
                    < Row key={index} className="text-center line">
                        <Col className="font-30">{item.funcao}</Col>
                        <Col className="font-30">{item.registro}</Col>
                    </Row>
                    < Row key={index} className="text-center line">
                        <Col className="font-30 preview-title">TERNO 1</Col>
                    </Row>
                </>
                
                )
            }else{
                return(
                    < Row key={index} className="text-center line">
                        <Col className="font-30">{item.funcao}</Col>
                        <Col className="font-30">{item.registro}</Col>
                    </Row>
                )
                
            }
            }
        
            
            
        
        
        )

    )
}



return(
        <Col md="4"  id="request" className="card-request mt-20 pt-20">
          <Card outline color="warning" >
            <CardBody   className="preview-title">
                <Row>
                    <Col md="8" className="font-30 align-self text-center">
                        {props.state.navio}
                    </Col>
                    <Col md="4" className="pl-3 text-center">
                        <Row className="font-60 justify-content-center text-center">{props.state.faina}</Row>
                    </Col>
                </Row>
                <Row>
                    <Col md="4" className="font-16 text-center">{props.state.carga}</Col>
                    <Col md="8" className="font-16 text-center">{props.state.operador}</Col>
                </Row>
           
            </CardBody>
            <CardBody className="c-body pt-0">
            {requestBody()}
            
            </CardBody>
        </Card>
        </Col>
)

}