import React from 'react';
import {Col, Row,} from 'reactstrap'
import myText from '../assets/demos/previa.txt'
import Request from '../components/Request/Request'

function Principal() {

//Estado que guarda as informações das Requisições  
const [request, setRequest] = React.useState({
  navio:"MV SARMIENTO",
  carga:"Soja",
  operador: "GI",
  faina:"17",
  terno:[
    {funcao:"AUX",registro:"2201"},
    {funcao:"OPC",registro:"2175"},
    {funcao:"OPC",registro:"2176"},
    {funcao:"ARR",registro:"2178"},
    {funcao:"ARR",registro:"2179"},
    {funcao:"ARR",registro:"2180"},
    
  ]})

  //Estado que armazena todas as requisições
  const [requestList, SetRequestList] = React.useState({elements:[]})
  
  const getRequests = () => {
    const updateRequest = require("../assets/demos/previa.json")
      SetRequestList({elements:updateRequest}) 
      console.log(request)
      console.log(updateRequest)
  }
  
  //Estado que atualiza as requisições
  React.useState(() => {
    //Função que busca o arquivo e atualiza os estados
    const updateRequest = require("../assets/demos/previa.json")
      SetRequestList({elements:updateRequest}) 
      console.log(request)
      console.log(updateRequest)
  }, []);

  
  return (
    
    <div id="principal" className="">
    
      <Row>
        <Col className="text-center">
        <h1>Prévia de Escalação</h1>
        </Col>
      </Row>
      <Row>
      {requestList.elements.map((item, index) => (
       
          <Request  state={item} />      

      ))}
    </Row>
      
    </div>
  );
}

export default Principal;
