import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Principal from './views/Principal';
import * as serviceWorker from './serviceWorker';
import './assets/scss/style.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <Principal />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
